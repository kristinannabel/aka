<?php
	include('session.php');
	
	$username = strstr($login_session, '@', true) ?: $login_session;
?>

<!DOCTYPE html>
<html>
	<head>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/stylesheet.css" rel="stylesheet">
		<title>AKA Administration page</title>
	</head>

	<body>
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		<?php include('mainMenu.php'); ?>
		<div class=col-md-3>
			<div class="list-group">
				<?php
					$conn = mysqli_connect("localhost", "aka_game", "higerdigg2015", "aka_game");

						// Check connection
						if (!$conn) {
							die("Connection failed: " . mysqli_connect_error());
						}
		
						$sql = "SELECT * FROM course";
						$result = $conn->query($sql);
						if($result->num_rows > 0){ //Token was added in database
							while($row = $result->fetch_assoc()) {
								$sql_dictionary = "SELECT * FROM dictionary WHERE course_id='".$row['id']."'";
								$dictionaryresult = $conn->query($sql_dictionary);
								echo "<a href='#' class='list-group-item courseButton' data-course-id=".$row['id']."><span class='badge'>".$dictionaryresult->num_rows."</span>". $row['name'] ."</a>";
							}
						}
		
				?>
			</div>
		</div>
		<div class="col-md-3 dictionaries"></div>
		<div class="col-md-6 dictionary_entries"></div>
		
		<script>
			$(".courseButton").click(function(){
				course_id = $(this).attr("data-course-id");
				var data = {
					"courseid": course_id
				};
				debugger;
				if($(this).find(".badge").html() > 0){
					$.ajax({
						type: "POST",
						url: "getDictionaries.php", 
						data: data,
						success: function(result){
		 		 		   	$(".dictionaries").html(result);
		 	   			}
					});
				}
			});
			$(document.body).on('click', '.dictionaryButton' ,function(){
			//$(".dictionaryButton").click(function(){
				debugger;
				dictionary_id = $(this).attr("data-dictionary-id");
				var data = {
					"dictionaryid": dictionary_id
				};
				debugger;
				if($(this).find(".badge").html() > 0){
					$.ajax({
						type: "POST",
						url: "getWords.php", 
						data: data,
						success: function(result){
		 		 		   	$(".dictionary_entries").html(result);
		 	   			}
					});
				}
			});
			
			
		</script>
	</body>

</html>