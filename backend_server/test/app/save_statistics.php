<?php
  
  $DB_SERVER="localhost";
  $DB_USER="aka_game";
  $DB_PASS="higerdigg2015";
  $DB_NAME="aka_game";  
  
  $entry_id = $_POST['dictionary_entry_id'];
  $user_id  = $_POST['user_id'];
  $time     = $_POST['time_spent'];
  $solved   = $_POST['solved'];

  $mysqli = new mysqli($DB_SERVER, $DB_USER, $DB_PASS, $DB_NAME); 
  
  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
  }
  
  $statement = $mysqli->prepare("INSERT INTO dictionary_entry_played (dictionary_entry_id, user_id, time_spent, solved) VALUES (?, ?, ?, ?)");
  
  $statement->bind_param('iiii', $entry_id, $user_id, $time, $solved); 
  $statement->execute();

  if ($statement->errno == 0) {
    echo json_encode(array("success"=>1, "inserted_id"=>$statement->insert_id));
  } else {
    echo json_encode(array("success"=>0, "errno"=>$statement->errno, "error"=>$statement->error));
  }
  

?>