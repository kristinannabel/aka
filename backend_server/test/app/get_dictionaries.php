<?php

  $DB_SERVER="localhost";
  $DB_USER="aka_game";
  $DB_PASS="higerdigg2015";
  $DB_NAME="aka_game";  

  $mysqli = new mysqli($DB_SERVER, $DB_USER, $DB_PASS, $DB_NAME); 

  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
  }
  
  $server_location ='http://46.246.93.110/test/app/dictionary_thumbnails/';
  $statement;
  $course_id = 1;// $_POST["course_id"];
  if (isset($course_id)) {
    $statement = $mysqli->prepare("SELECT id, name, course_id, image FROM dictionary WHERE course_id = ?");
    $statement->bind_param('s', $course_id);  
  } else {
    $statement = $mysqli->prepare("SELECT id, name, course_id, image FROM dictionary");
  }
    
  $statement->execute();
  $statement->bind_result($id, $name, $course, $image);

  // Fetch values

  $dictionary; 
  $dictionary_counter = 0; 
  while($statement->fetch()) {  
  
    // Set imageurl only if image exists:
    $imageurl = "";
    if(isset($image)){
      $imageurl = $server_location . $image;
    }

    $dictionary[$dictionary_counter]["id"] = $id;
    $dictionary[$dictionary_counter]["name"] = $name;
    $dictionary[$dictionary_counter]["course_id"] = $course;
    $dictionary[$dictionary_counter]["image"] = $imageurl;
    
    $dictionary_counter += 1;

  }
  // Escape "\/ in URLs after the json_encode":
  echo str_replace('\\/', '/', json_encode($dictionary));
?>