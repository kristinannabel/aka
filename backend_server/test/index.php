<?php
	include('login.php'); // Includes Login Script

	if(isset($_SESSION['login_user'])){
		header("location: admin.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/stylesheet.css" rel="stylesheet">
		<title>AKA Administration page</title>
	</head>

	<body>
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		<div class="container">
			<form class="form-signin" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        		<h2 class="form-signin-heading">Please sign in</h2>
        			<label for="inputEmail" class="sr-only">Email address</label>
        			<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        			<label for="inputPassword" class="sr-only">Password</label>
        			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
        			<div class="checkbox">
          		  		<label>
            				<input type="checkbox" name="remember" value="remember-me"> Remember me
          				</label>
        			</div>
        			<button class="btn btn-lg btn-primary btn-block login-btn" name="submit" type="submit">Sign in</button>
					<span class="login-error"><?php echo $error; ?></span>
      		</form>
		</div>
		
		<script>
			
		</script>
	</body>
</html>