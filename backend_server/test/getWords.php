<?php
if(isset($_POST['dictionaryid'])){
	$dictionary_id = $_POST['dictionaryid'];
	$html = "";
	
	$conn = mysqli_connect("localhost", "aka_game", "higerdigg2015", "aka_game");

	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	
	$html .= "<table class='table table-bordered'><thead><tr><th>#</th><th>Word</th><th>Hint 1</th><th>Hint 2</th><th>Hint 3</th></tr></thead><tbody>";
	$sql = "SELECT * FROM dictionary_entry WHERE dictionary_id='".$dictionary_id."'";
	$result = $conn->query($sql);
	if($result->num_rows > 0){ //Token was added in database
		while($row = $result->fetch_assoc()) {
			$html .= "<tr><th scope='row'>".$row['id']."</th><td>".$row['word']."</td>";
			
			$sql_hint = "SELECT * FROM hint WHERE word_id='".$row['id']."'";
			$hintresult = $conn->query($sql_hint);
			if($hintresult->num_rows > 0){ //Token was added in database
				while($hintrow = $hintresult->fetch_assoc()) {
					$html .= "<td>".$hintrow['hint']."</td>";
				}
			}
			else {
				$html .= "<td></td><td></td><td></td>";
			}
			if($hintresult->num_rows == 1){
				$html .= "<td></td><td></td>";
			}
			else if($hintresult->num_rows == 2){
				$html .= "<td></td>";
			}
			$html .= "</tr>";
		}
	}
	$html .= "</tbody></table>";
	echo $html;
}
?>