﻿using UnityEngine;
using System.Collections;

public class PlayMenuManager : MonoBehaviour {




	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnGUI () {
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 20;

		// Make a background box
		GUI.Box(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 120), "Round Menu");

		
		// Make the second button.
		if(GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 - 50, 120, 50), "Play", myButtonStyle)) {
			Application.LoadLevel("playing");
		}
	}
}