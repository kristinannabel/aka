﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class ChooseDictionaryManager : MonoBehaviour {
	public Texture AnatomiTexture;

	//my JSON string
	public JSONNode D;

	int numOfDictionaries = 0;

	//Dictionary names
	string dict1;
	string dict2;
	string dict3;
	string dict4;

	//Counter for viewed dictionaries
	int dictionaryCounter = 0;

	// Use this for initialization
	IEnumerator Start () {
		string url = "http://46.246.93.110/test/app/get_dictionaries.php";
		WWW dictionaries = new WWW (url);
		yield return dictionaries;
		
		if (!string.IsNullOrEmpty (dictionaries.error)) { //if error with download
			Debug.Log (dictionaries.error);
		} else {
			D = JSON.Parse(dictionaries.text);
			numOfDictionaries = D.Count;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		GUIStyle myModeStyle = new GUIStyle (GUI.skin.label);
		myModeStyle.fontSize = 15;
		myModeStyle.alignment = TextAnchor.MiddleCenter;
		GUIStyle myHeaderStyle = new GUIStyle (GUI.skin.label);
		myHeaderStyle.fontSize = 40;
		GUIStyle myHintStyle = new GUIStyle (GUI.skin.button);
		myHintStyle.alignment = TextAnchor.MiddleLeft;
		GUIStyle myButtonStyle = new GUIStyle (GUI.skin.button);
		myButtonStyle.alignment = TextAnchor.MiddleCenter;

		GUI.Label (new Rect (Screen.width / 2 - 110, Screen.height / 2 - 150, 300, 50), "Velg ordliste", myHeaderStyle);

		switch (numOfDictionaries - dictionaryCounter) {
		case 0:
			break;
		case 1:
			dict1 = D [dictionaryCounter] ["name"];
		if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2, 160, 50), dict1, myModeStyle);
			break;
		case 2:
			dict1 = D [dictionaryCounter] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2, 160, 50), dict1, myModeStyle);

			dict2 = D [dictionaryCounter + 1] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 20, Screen.height / 2, 160, 50), dict2, myModeStyle);
			break;
		case 3:
			dict1 = D [dictionaryCounter] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2, 160, 50), dict1, myModeStyle);

			dict2 = D [dictionaryCounter + 1] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 20, Screen.height / 2, 160, 50), dict2, myModeStyle);

			dict3 = D [dictionaryCounter + 2] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 + 50, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2 + 130, 160, 50), dict3, myModeStyle);
			break;
		default:
			dict1 = D [dictionaryCounter] ["name"];
		if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2, 160, 50), dict1, myModeStyle);
		
			dict2 = D [dictionaryCounter + 1] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 - 80, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 20, Screen.height / 2, 160, 50), dict2, myModeStyle);
		
			dict3 = D [dictionaryCounter + 2] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 + 50, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 160, Screen.height / 2 + 130, 160, 50), dict3, myModeStyle);

			dict4 = D [dictionaryCounter + 3] ["name"];
			if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 + 50, 120, 120), AnatomiTexture, myHintStyle)) {
				Application.LoadLevel ("startRoundMenu");
			}
			GUI.Label (new Rect (Screen.width / 2 - 20, Screen.height / 2 + 130, 160, 50), dict4, myModeStyle);

			break;
		}
		if (dictionaryCounter > 0) { //Has gone atleast one step forward
			//Previous-button
			if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 + 200, 120, 30), "Previous", myButtonStyle)) {
				dictionaryCounter -= 4;
			}
		}
	
		if ((numOfDictionaries - dictionaryCounter) > 4) { //if there is more dictionaries
			//Next-button
			if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 + 200, 120, 30), "Next", myButtonStyle)) {
				dictionaryCounter += 4;
			}
		}
	}
}
