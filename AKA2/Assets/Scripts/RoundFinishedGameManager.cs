﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoundFinishedGameManager : MonoBehaviour {
	List<GameManagerScript.Word> answers = GameManagerScript.arrayOfAnswers;
	string formText = "";
	public string text;

	// Use this for initialization
	void Start () {
		SendScore ();
	}

	IEnumerable SendScore() {
		string URL = "http://46.246.93.110/test/app/save_statistics.php";
		for (int i = 0; i < answers.Count; i++) {
			GameManagerScript.Word currentWord = answers[i];
			int correct = 0;
			if(currentWord.correct){
				correct = 1;
			}

			WWWForm form = new WWWForm(); //here you create a new form connection
			form.AddField( "dictionary_entry_id", currentWord.id ); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
			form.AddField( "user_id", 0 );
			form.AddField( "time_spent", currentWord.timeSpent );
			form.AddField( "solved", correct );
			WWW w = new WWW(URL, form); //here we create a var called 'w' and we sync with our URL and the form
			yield return w; //we wait for the form to check the PHP file, so our game dont just hang
			if (w.error != null) {
				print(w.error); //if there is an error, tell us
				text = w.error;
			} else {
				print("Test ok");
				formText = w.data; //here we return the data our PHP told us
				text = w.data;
				w.Dispose(); //clear our form in game
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		int finalPoints = GameManagerScript.playersPoints;


		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 20;
		GUIStyle myLabelStyle = new GUIStyle (GUI.skin.label);
		myLabelStyle.fontSize = 10;
		myLabelStyle.alignment = TextAnchor.MiddleCenter;

		// Make a background box
		GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 400, 220), "");
		if (GameManagerScript.emptyWordList == true) { //If we ran out of words
			GUI.Label (new Rect (Screen.width / 2 - 100, Screen.height / 2 - 80, 200, 80), "Ran out of words to play!" + text, myLabelStyle);
		} else {
			GUI.Label (new Rect (Screen.width / 2 - 100, Screen.height / 2 - 80, 200, 80), "Round done!", myLabelStyle);
		}

		GUI.Label (new Rect (Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 80), "You got " + finalPoints + " points!", myLabelStyle);

		// Make the second button.
		if(GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height / 2 + 30, 120, 50), "Play again!", myButtonStyle)) {
			GameManagerScript.playersPoints = 0;
			Application.LoadLevel("playing");
		}

	}
}
