using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class GameManagerScript : MonoBehaviour {
	public class Word
	{
		public int id;
		public bool correct;
		public int timeSpent;

		public Word(int id, bool correct, int timeSpent){
			this.id = id;
			this.correct = correct;
			this.timeSpent = timeSpent;
		}

		public string toString(){
			return id + " " + correct + " " + timeSpent;
		}
	}

	public GameObject Card1;
	//public GameObject Card2;
	bool hasError;
	string sometext = "Offline";
	string textTime;
	public float roundTimer = 60;
	string MainWord = "";

	//my JSON string
	public JSONNode N;

	//The three hints
	string hint1 = "";
	string hint2 = "";
	string hint3 = "";

	int hintCounter = 0;
	public static int playersPoints = 0;
	public int wordCounter = 0;

	public static List<Word> arrayOfAnswers = new List<Word>();

	public static bool emptyWordList = false;

	void Awake() {
		//DontDestroyOnLoad(transform.gameObject);
	}

	// Use this for initialization
	IEnumerator Start () {
		string url = "http://46.246.93.110/test/app/get_words.php";
		WWW www = new WWW (url);
		yield return www;

		if (!string.IsNullOrEmpty (www.error)) { //if error with download
			hasError = true;
			Debug.Log (www.error);
			sometext = "Offline";
		} else {
			hasError = false;
			sometext = "Online";
			N = JSON.Parse(www.text);

			if(wordCounter < N.Count){
				MainWord = N[wordCounter]["word"];
			}
			else {
				emptyWordList = true;
				Application.LoadLevel("roundFinished");
			}

			Card1.GetComponent<SwipeController> ().topCard = true;
		}
	}

	// Update is called once per frame
	void Update () {
		//guiTime = Time.deltaTime + guiTime;
		if(roundTimer > 0){
			roundTimer -= Time.deltaTime;
		}
		if(roundTimer <= 0){
			Application.LoadLevel("roundFinished");
		}
	}

	void OnGUI () {
		GUIStyle myMainWordStyle = new GUIStyle (GUI.skin.label);
		myMainWordStyle.fontSize = 30;
		myMainWordStyle.alignment = TextAnchor.MiddleCenter;
		myMainWordStyle.normal.textColor = Color.black;
		GUI.Label (new Rect (Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 60), MainWord, myMainWordStyle);

		float seconds = roundTimer % 60;
		float minutes = roundTimer / 60;
		float fraction = (roundTimer * 100) % 100;
		textTime = string.Format("{0:00}:{1:00}:{2:00}", 0, seconds, fraction); 
		GUI.Label (new Rect (400, 25, 100, 30), textTime);

		//Hint words
		GUIStyle myHintWords = new GUIStyle (GUI.skin.label);
		myHintWords.normal.textColor = Color.blue;
		myHintWords.active.textColor = Color.blue;
		myHintWords.focused.textColor = Color.blue;
		myHintWords.hover.textColor = Color.blue;
		myHintWords.fontSize = 20;
		myHintWords.alignment = TextAnchor.MiddleCenter;

		GUI.Label (new Rect (Screen.width / 2 - 55, Screen.height / 2, 120, 50), hint1, myHintWords);
		GUI.Label (new Rect (Screen.width / 2 - 55, Screen.height / 2 + 20, 120, 50), hint2, myHintWords);
		GUI.Label (new Rect (Screen.width / 2 - 55, Screen.height / 2 + 40, 120, 50), hint3, myHintWords);

		//Hint-button
		GUIStyle myHintButton = new GUIStyle (GUI.skin.button);
		myHintButton.fontSize = 20;
		myHintButton.normal.textColor = Color.black;
		myHintButton.active.textColor = Color.black;
		myHintButton.focused.textColor = Color.black;
		myHintButton.hover.textColor = Color.black;

		//SHOULD DO THIS ANOTHER WAY - NEEDS TO BE FIXED
		if (GUI.Button (new Rect (Screen.width / 2 - 55, Screen.height / 2 - 60, 120, 50), "Hint", myHintButton)) {
			hintCounter++;
			//Hint-button is clicked

			if(hintCounter == 1){
				hint1 = N[wordCounter]["hint"][0];
			}
			else if(hintCounter == 2){
				hint1 = N[wordCounter]["hint"][0];
				hint2 = N[wordCounter]["hint"][1];
			}
			else if(hintCounter >= 3){
				hint1 = N[wordCounter]["hint"][0];
				hint2 = N[wordCounter]["hint"][1];
				hint3 = N[wordCounter]["hint"][2];
				// FIND SOME WAY TO DISABLE/REMOVE HINT-BUTTON HERE
			}
		}


		// Pass-button
		GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button);
		myButtonStyle.fontSize = 20;
		myButtonStyle.normal.textColor = Color.red;
		myButtonStyle.active.textColor = Color.red;
		myButtonStyle.focused.textColor = Color.red;
		myButtonStyle.hover.textColor = Color.red;
		if (GUI.Button (new Rect (Screen.width / 2 - 140, Screen.height / 2 + 220, 90, 70), "Pass", myButtonStyle)) {
			//Pass is clicked
			playersPoints--;
			int oldWordId = N[wordCounter]["id"].AsInt;
			arrayOfAnswers.Add(new Word(oldWordId, false, 0));

			hintCounter = 0;
			wordCounter++;
			if(wordCounter < N.Count){
				MainWord = N[wordCounter]["word"];
			}
			else {
				emptyWordList = true;
				Application.LoadLevel("roundFinished");
			}
			hint1 = "";
			hint2 = "";
			hint3 = "";
		}

		//Right-button
		GUIStyle myRightButtonStyle = new GUIStyle(GUI.skin.button);
		myRightButtonStyle.fontSize = 20;
		myRightButtonStyle.normal.textColor = Color.green;
		myRightButtonStyle.active.textColor = Color.green;
		myRightButtonStyle.focused.textColor = Color.green;
		myRightButtonStyle.hover.textColor = Color.green;
		if (GUI.Button (new Rect (Screen.width / 2 + 60, Screen.height / 2 + 220, 90, 70), "Riktig", myRightButtonStyle)) {
			//Right is clicked
			playersPoints++;
			int oldWordId = N[wordCounter]["id"].AsInt;
			arrayOfAnswers.Add(new Word(oldWordId, true, 0));

			hintCounter = 0;
			wordCounter++;
			if(wordCounter < N.Count){
				MainWord = N[wordCounter]["word"];
			}
			else {
				emptyWordList = true;
				Application.LoadLevel("roundFinished");
			}
			hint1 = "";
			hint2 = "";
			hint3 = "";
		}

		if(GUI.Button(new Rect(20,40,80,20), sometext)) {
			Start ();

			if (hasError == true) { //if error with download
				sometext = "Offline";
			}
			else {
				sometext = "Online";
			}
		}
	}
}
