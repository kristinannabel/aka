﻿using UnityEngine;
using System.Collections;

public class ChooseGameModeManager : MonoBehaviour {
	public Texture GameModeTexture;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		GUIStyle myModeStyle = new GUIStyle(GUI.skin.label);
		myModeStyle.fontSize = 30;
		GUIStyle myHeaderStyle = new GUIStyle (GUI.skin.label);
		myHeaderStyle.fontSize = 40;
		
		GUI.Label(new Rect (Screen.width / 2 - 135, Screen.height / 2 - 150, 300, 50), "Velg spill-modus", myHeaderStyle);
		
		if(GUI.Button(new Rect(Screen.width / 2 - 95, Screen.height / 2 - 50, 200, 200), GameModeTexture)){
			Application.LoadLevel("chooseDictionary");
		}
		
		GUI.Label (new Rect (Screen.width / 2 - 60, Screen.height / 2 + 110, 150, 50), "Education", myModeStyle);

	}
}
