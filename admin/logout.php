<?php
	session_start();
	if(session_destroy()) // Destroying All Sessions
	{
		setcookie("RememberUser", "", time()-3600);
		header("Location: index.php"); // Redirecting To Home Page
	}
?>